var options = {
    animation: true,
    animationEasing: "easeInOutQuart",
    segmentShowStroke : false,
    showTooltips: false
};

var data = [
    {
        value: 300,
        color:"#3b5998",
        highlight: "#3b5998",
        label: "Positivos"
    },
    {
        value: 120,
        color: "#9daccb",
        highlight: "#9daccb",
        label: "Neutros"
    },
    {
        value: 100,
        color: "#768bb7",
        highlight: "#768bb7",
        label: "Negativos"
    }
];

$(document).ready(function(){
  var ctx = document.getElementById("Grafico").getContext("2d");
  var myNewChart = new Chart(ctx).Pie(data,options);
});